const express = require("express");
const router = express.Router();
const passport = require("../lib/passport");
const gameplayControllers = require("../controllers/gameplay");

router.post('/join', passport.authenticate("jwt", { session : false }),gameplayControllers.join )

module.exports = router

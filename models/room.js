'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
 
    static associate(models) {
      models.Room.hasMany(models.RoomPlayer, {
        foreignKey: "roomId",
      })
    }
  }
  Room.init({
    winnerId: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    status: {
      type : DataTypes.ENUM(['OPEN', "IN_PROGRESS", "CLOSED"]),
      allowNull: false,
      defaultValue: "OPEN",
    }
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};